﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Vive.Plugin.SR;

using OpenCVForUnity.CoreModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.ArucoModule;
using OpenCVForUnity.Calib3dModule;

public class MarkerDetection : MonoBehaviour
{

    public GameObject opencvOutputObject;
    public GameObject arObject;
    public float markerSize = 0.18f;

    public float markerSeparationSize = 0.01f;

    public bool useGridBoard = true;

    // texture references 
    private Texture2D imgTextureLeft;
    private Texture2D imgTextureRight;
    private Texture2D readableTexture;

    // status of initialization
    // firstWorking: inizialize values based on ViveSR_DualCameraImageCapture
    // initialWaited: after DualCameraRig is working it takes about 1 second to deliver the first usable image
    private bool firstWorking = false;
    private bool initialWaited = false;


    // OpenCV Marker Detection needed Variables
    private Dictionary dictionary;
    private List<Mat> corners;
    private Mat ids;
    private DetectorParameters detectorParams;
    private List<Mat> rejectedCorners;
    private Mat camMatrix;
    private MatOfDouble distCoeffs;

    private GridBoard gridBoard;

    // Start is called before the first frame update
    void Start()
    {
        // We are using the undistorted images from SRWork therfore the distCoeffs are zero
        distCoeffs = new MatOfDouble(0, 0, 0, 0);

        // initialize Aruco Parameters
        // we are using the Marker from the OpenCV examples
        // Assets\OpenCVForUnity\Examples\Resources\ar_markers\CanonicalMarker-d10-i1-sp500-bb1.pdf
        detectorParams = DetectorParameters.create();
        dictionary = Aruco.getPredefinedDictionary(Aruco.DICT_6X6_250);
        gridBoard = GridBoard.create(3, 5, markerSize, markerSeparationSize, dictionary, 0);

    }

    // Wait for about 1 second to ensure a usable image is deliverd from the CameraImageCapture
    IEnumerator WaitOnStart()
    {
        yield return new WaitForSeconds(1);

        ViveSR_DualCameraRig.Instance.Mode = DualCameraDisplayMode.VIRTUAL;
        initialWaited = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (ViveSR_DualCameraRig.DualCameraStatus == DualCameraStatus.WORKING)
        {
            // if DualCamera is working we can initialize some parameters and then should wait around one second for the camera to deliver a image
            if (firstWorking == false)
            {
                firstWorking = true;

                imgTextureLeft = new Texture2D(ViveSR_DualCameraImageCapture.UndistortedImageWidth, ViveSR_DualCameraImageCapture.UndistortedImageHeight, TextureFormat.RGBA32, false);
                imgTextureRight = new Texture2D(ViveSR_DualCameraImageCapture.UndistortedImageWidth, ViveSR_DualCameraImageCapture.UndistortedImageHeight, TextureFormat.RGBA32, false);
                readableTexture = new Texture2D(ViveSR_DualCameraImageCapture.UndistortedImageWidth, ViveSR_DualCameraImageCapture.UndistortedImageHeight, TextureFormat.RGBA32, false);

                // create Camera Martix based on the intrinsic camera parameters (we are using undistorted images)

                // Camera Matrix:
                // fx   0   cx
                // 0    fy  cy
                // 0    0   1
                // reference: https://forum.vive.com/topic/5966-marker-based-ar-with-srworks/
                camMatrix = new Mat(3, 3, CvType.CV_64FC1);
                camMatrix.put(0, 0, ViveSR_DualCameraImageCapture.FocalLengthLeft);
                camMatrix.put(0, 1, 0);
                camMatrix.put(0, 2, ViveSR_DualCameraImageCapture.UndistortedCxLeft);
                camMatrix.put(1, 0, 0);
                camMatrix.put(1, 1, ViveSR_DualCameraImageCapture.FocalLengthLeft);
                camMatrix.put(1, 2, ViveSR_DualCameraImageCapture.UndistortedCyLeft);
                camMatrix.put(2, 0, 0);
                camMatrix.put(2, 1, 0);
                camMatrix.put(2, 2, 1.0f);

                // wait one aditional Second 
                StartCoroutine(WaitOnStart());
            }

            // wating until the initial wait time of 1 second is over
            if (initialWaited)
            {
                // Debug functionality - Switch between virtual and mixed mode of CameraRig by pressing "space"
                // Mix: show virtual objects and real environment
                // Virtual: show only virtual objects
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (ViveSR_DualCameraRig.Instance.Mode == DualCameraDisplayMode.VIRTUAL)
                        ViveSR_DualCameraRig.Instance.Mode = DualCameraDisplayMode.MIX;
                    else
                        ViveSR_DualCameraRig.Instance.Mode = DualCameraDisplayMode.VIRTUAL;
                }

                // declare out parameters for GetUndistortedTexture
                // these variables are not used in the later (only working with texture of the left image)
                int frameIndex, timeIndex;
                Matrix4x4 poseLeft, poseRight;
                ViveSR_DualCameraImageCapture.GetUndistortedTexture(out imgTextureLeft, out imgTextureRight, out frameIndex, out timeIndex, out poseLeft, out poseRight);


                // convert the imgTextureLeft to a readable Texture
                // the left image Texture can be externally created and therfor has no data to read directly
                // Using a RenderTexture to create a readable Texture
                RenderTexture tmp = RenderTexture.GetTemporary(imgTextureLeft.width, imgTextureLeft.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
                Graphics.Blit(imgTextureLeft, tmp);
                RenderTexture previous = RenderTexture.active;
                RenderTexture.active = tmp;
                readableTexture.ReadPixels(new UnityEngine.Rect(0, 0, tmp.width, tmp.height), 0, 0);
                RenderTexture.active = previous;
                RenderTexture.ReleaseTemporary(tmp);


                // convert texture to Opencv Mat
                Mat imgMat = new Mat(readableTexture.height, readableTexture.width, CvType.CV_8UC4);
                Utils.fastTexture2DToMat(readableTexture, imgMat, false); // no flipping

                // convert RGBA to RGB for marker detection
                Imgproc.cvtColor(imgMat, imgMat, Imgproc.COLOR_RGBA2RGB);

                ids = new Mat();
                corners = new List<Mat>();
                rejectedCorners = new List<Mat>();
                Mat rvecs = new Mat();
                Mat tvecs = new Mat();

                // Marker Detection
                Aruco.detectMarkers(imgMat, dictionary, corners, ids, detectorParams, rejectedCorners, camMatrix, distCoeffs);

                // Output the number of found markers
                Debug.Log(ids.total());

                // if at least one marker detected
                if (ids.total() > 0)
                {
                    // in the opencv image draw the corners of the detectet Marker with green (for debug purposes)
                    Aruco.drawDetectedMarkers(imgMat, corners, ids, new Scalar(0, 255, 0));
                    // if the marker is not displayed there look at the rejectedCorners

                    if (useGridBoard)
                    {

                        Mat rvec = new Mat();
                        Mat tvec = new Mat();
                        int valid = Aruco.estimatePoseBoard(corners, ids, gridBoard, camMatrix, distCoeffs, rvec, tvec);

                        // if at least one board marker detected
                        if (valid > 0)
                        {
                            // In this example we are processing with RGB color image, so Axis-color correspondences are X: blue, Y: green, Z: red. (Usually X: red, Y: green, Z: blue)
                            Calib3d.drawFrameAxes(imgMat, camMatrix, distCoeffs, rvec, tvec, markerSize);
                             UpdataARMarker(rvec, tvec);
                        }
                    }
                    else
                    {
                        // estimate the pose of the markers
                        Aruco.estimatePoseSingleMarkers(corners, markerSize, camMatrix, distCoeffs, rvecs, tvecs);

                        // 
                        for (int i = 0; i < ids.total(); i++)
                        {
                            using (Mat rvec = new Mat(rvecs, new OpenCVForUnity.CoreModule.Rect(0, i, 1, 1)))
                            using (Mat tvec = new Mat(tvecs, new OpenCVForUnity.CoreModule.Rect(0, i, 1, 1)))
                            {
                                // In this example we are processing with RGB color image, so Axis-color correspondences are X: blue, Y: green, Z: red. (Usually X: red, Y: green, Z: blue)
                                Calib3d.drawFrameAxes(imgMat, camMatrix, distCoeffs, rvec, tvec, markerSize / 2);

                                // This example can display the ARObject on only first detected marker.
                                if (i == 0)
                                {
                                    UpdataARMarker(rvec, tvec);
    
                                // // Convert to unity pose data.
                                // double[] rvecArr = new double[3];
                                // rvec.get(0, 0, rvecArr);
                                // double[] tvecArr = new double[3];
                                // tvec.get(0, 0, tvecArr);
                                // PoseData poseData = ARUtils.ConvertRvecTvecToPoseData(rvecArr, tvecArr);

                                    // Matrix4x4 transformation = Matrix4x4.TRS(poseData.pos, poseData.rot, Vector3.one);

                                    // // right-handed coordinates system (OpenCV) to left-handed one (Unity)
                                    // // https://stackoverflow.com/questions/30234945/change-handedness-of-a-row-major-4x4-transformation-matrix

                                    // // Matrix for inverting the Y axis 
                                    // Matrix4x4 invertYM = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, -1, 1));                            
                                    // Matrix4x4 ARM = invertYM * transformation * invertYM;

                                    // // ARM is the transformaiton from marker coordinate system to the camera coordinate system
                                    // // https://docs.opencv.org/4.3.0/d9/d6a/group__aruco.html#ga84dd2e88f3e8c3255eb78e0f79571bd1

                                    // Vector3 localPos = ARUtils.ExtractTranslationFromMatrix(ref ARM);
                                    // Quaternion localRot = ARUtils.ExtractRotationFromMatrix(ref ARM);

                                    // // assume the arObject is a child of the left camera or an object with the same position and rotation
                                    // arObject.transform.localPosition = localPos;
                                    // arObject.transform.localRotation = localRot;
                                }
                            }
                        }
                    }
                }

                // create texture from OpenCV Mat for debug purpose
                Texture2D debugOutputTexture = new Texture2D(ViveSR_DualCameraImageCapture.UndistortedImageWidth, ViveSR_DualCameraImageCapture.UndistortedImageHeight, TextureFormat.RGB24, false);
                Utils.fastMatToTexture2D(imgMat, debugOutputTexture);
                opencvOutputObject.GetComponent<Renderer>().material.mainTexture = debugOutputTexture;
            }
        }
    }


    private void UpdataARMarker(Mat rvec, Mat tvec)
    {
        // Convert to unity pose data.
        double[] rvecArr = new double[3];
        rvec.get(0, 0, rvecArr);
        double[] tvecArr = new double[3];
        tvec.get(0, 0, tvecArr);
        PoseData poseData = ARUtils.ConvertRvecTvecToPoseData(rvecArr, tvecArr);

        Matrix4x4 transformation = Matrix4x4.TRS(poseData.pos, poseData.rot, Vector3.one);

        // right-handed coordinates system (OpenCV) to left-handed one (Unity)
        // https://stackoverflow.com/questions/30234945/change-handedness-of-a-row-major-4x4-transformation-matrix

        // Matrix for inverting the Y axis 
        Matrix4x4 invertYM = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1, -1, 1));
        Matrix4x4 ARM = invertYM * transformation * invertYM;

        // ARM is the transformaiton from marker coordinate system to the camera coordinate system
        // https://docs.opencv.org/4.3.0/d9/d6a/group__aruco.html#ga84dd2e88f3e8c3255eb78e0f79571bd1

        Vector3 localPos = ARUtils.ExtractTranslationFromMatrix(ref ARM);
        Quaternion localRot = ARUtils.ExtractRotationFromMatrix(ref ARM);

        // assume the arObject is a child of the left camera or an object with the same position and rotation
        arObject.transform.localPosition = localPos;
        arObject.transform.localRotation = localRot;
    }
}
