﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyPose : MonoBehaviour
{

    public GameObject reference;

    public bool mixedRealityMode;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (reference != null)
        {
            // if (Vive.Plugin.SR.ViveSR_DualCameraRig.Instance.Mode != Vive.Plugin.SR.DualCameraDisplayMode.VIRTUAL)
            // {
                this.transform.localRotation = reference.transform.localRotation;
                this.transform.localPosition = reference.transform.localPosition;
            // }
        }
    }
}

